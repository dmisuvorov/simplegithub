package com.media.dmitry68.simplegithub.data.login;

import com.media.dmitry68.simplegithub.di.LoginScope;
import com.media.dmitry68.simplegithub.domain.BasicUserAuth;
import com.media.dmitry68.simplegithub.domain.User;

import javax.inject.Inject;

import io.reactivex.Single;

@LoginScope
public class LoginRepositoryImpl implements LoginRepository {
    @Inject
    LoginDataSource loginDataSource;

    @Inject
    public LoginRepositoryImpl() {

    }

    @Override
    public Single<User> login(String login, String password) {
        return loginDataSource.login(login, password);
    }

    @Override
    public Single<BasicUserAuth> getSavedAuth() {
        return loginDataSource.getUserBasicAuth();
    }

    @Override
    public void putAuthenticatedUser(String login, String password) {
        loginDataSource.putAuthenticatedUser(login, password);
    }
}
