package com.media.dmitry68.simplegithub.ui.content.list.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.media.dmitry68.simplegithub.presentation.list.RepoListPresenter;

public class OnRepoScrollListener extends RecyclerView.OnScrollListener {
    static boolean repoScrollLoading = true;
    private LinearLayoutManager linearLayoutManager;
    private RepoListPresenter repoListPresenter;

    public OnRepoScrollListener(LinearLayoutManager linearLayoutManager, RepoListPresenter repoListPresenter) {
        this.linearLayoutManager = linearLayoutManager;
        this.repoListPresenter = repoListPresenter;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy > 0) {
            int totalItemsCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
            int visibleItemsCount = linearLayoutManager.getChildCount();
            if (repoScrollLoading && visibleItemsCount + firstVisibleItemPosition >= totalItemsCount) {
                repoScrollLoading = false;
                repoListPresenter.setNextRepositories();
            }
        }
    }

    public interface OnScrollListCallback {
        void onScrollList();
    }
}
