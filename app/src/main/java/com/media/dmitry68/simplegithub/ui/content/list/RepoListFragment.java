package com.media.dmitry68.simplegithub.ui.content.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.base.BaseFragment;
import com.media.dmitry68.simplegithub.domain.Repositories;
import com.media.dmitry68.simplegithub.presentation.list.RepoListPresenter;
import com.media.dmitry68.simplegithub.ui.content.list.adapter.OnRepoScrollListener;
import com.media.dmitry68.simplegithub.ui.content.list.adapter.RepoListAdapter;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;


public class RepoListFragment extends BaseFragment implements RepoListView, RepoListAdapter.AdapterClickListener {
    private static final String ARG_REPOSITORIES = "repositoriesModel";

    private RecyclerView listOfRepositories;

    private OnRepoScrollListener.OnScrollListCallback scrollListCallback;
    private RepoListFragmentCallback repoListFragmentCallback;

    @Inject
    RepoListAdapter adapter;

    @Inject
    RepoListPresenter presenter;

    @Inject
    LinearLayoutManager layoutManager;

    @Inject
    OnRepoScrollListener scrollListener;


    public RepoListFragment() {

    }

    public static RepoListFragment newInstance(Repositories repositories) {
        RepoListFragment repoListFragment = new RepoListFragment();
        Bundle repositoriesBundle = new Bundle();
        repositoriesBundle.putParcelableArrayList(ARG_REPOSITORIES, repositories.getList());
        repoListFragment.setArguments(repositoriesBundle);
        return repoListFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.bind(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_repo_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        listOfRepositories = Objects.requireNonNull(getView()).findViewById(R.id.repo_list);
        if (getArguments() != null) {
            initViews(getArguments().getParcelableArrayList(ARG_REPOSITORIES));
        }
    }

    private void initViews(List<Repositories.Item> repositories) {
        if (getActivity() instanceof OnRepoScrollListener.OnScrollListCallback) {
            scrollListCallback = (OnRepoScrollListener.OnScrollListCallback) getActivity();
        }
        if (getActivity() instanceof RepoListFragmentCallback) {
            repoListFragmentCallback = (RepoListFragmentCallback) getActivity();
        }
        setRepositories(repositories);
        listOfRepositories.setAdapter(adapter);
        listOfRepositories.setLayoutManager(layoutManager);
        listOfRepositories.addOnScrollListener(scrollListener);
    }

    @Override
    protected void injectDependencies(App application) {
        App.getListRepoSubComponent(this).inject(this);
    }

    @Override
    public void setRepositories(List<Repositories.Item> repositories) {
        adapter.setList(repositories);
    }

    @Override
    public void requestNextRepositories() {
        if (scrollListCallback != null) {
            scrollListCallback.onScrollList();
        }
    }

    @Override
    public void nextRepositories(List<Repositories.Item> repositories) {
        adapter.addList(repositories);
    }

    @Override
    public void onItemClicked(Repositories.Item repository) {
        if (repoListFragmentCallback != null) {
            repoListFragmentCallback.showDetailRepository(repository);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.unbind();
    }

    public interface RepoListFragmentCallback {
        void showDetailRepository(Repositories.Item repository);
    }

}
