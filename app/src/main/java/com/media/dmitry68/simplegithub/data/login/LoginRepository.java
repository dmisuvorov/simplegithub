package com.media.dmitry68.simplegithub.data.login;

import com.media.dmitry68.simplegithub.domain.BasicUserAuth;
import com.media.dmitry68.simplegithub.domain.User;

import io.reactivex.Single;

public interface LoginRepository {
    Single<BasicUserAuth> getSavedAuth();

    Single<User> login(String username, String password);

    void putAuthenticatedUser(String login, String password);
}
