package com.media.dmitry68.simplegithub.di;


import com.media.dmitry68.simplegithub.di.modules.LoginModule;
import com.media.dmitry68.simplegithub.ui.login.LoginActivity;

import dagger.Subcomponent;

@LoginScope
@Subcomponent(modules = LoginModule.class)
public interface LoginSubComponent {

    void inject(LoginActivity activity);

}
