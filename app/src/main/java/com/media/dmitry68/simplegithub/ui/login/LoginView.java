package com.media.dmitry68.simplegithub.ui.login;

import com.media.dmitry68.simplegithub.domain.User;

public interface LoginView {
    void setLogin(String login);

    void setPassword(String password);

    void onClickLoginButton();

    void showProgress();

    void dismissProgress();

    void loginSuccess(User user);

    void loginFail();
}
