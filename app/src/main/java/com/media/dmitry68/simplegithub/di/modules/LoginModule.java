package com.media.dmitry68.simplegithub.di.modules;

import com.media.dmitry68.simplegithub.data.login.LoginDataSource;
import com.media.dmitry68.simplegithub.data.login.LoginRepository;
import com.media.dmitry68.simplegithub.data.login.LoginRepositoryImpl;
import com.media.dmitry68.simplegithub.di.LoginScope;
import com.media.dmitry68.simplegithub.framework.login.LoginDataSourceImpl;
import com.media.dmitry68.simplegithub.interactors.Login;
import com.media.dmitry68.simplegithub.interactors.LoginImpl;
import com.media.dmitry68.simplegithub.presentation.login.LoginPresenter;
import com.media.dmitry68.simplegithub.presentation.login.LoginPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class LoginModule {

    @Provides
    @LoginScope
    public LoginPresenter provideLoginPresenter(LoginPresenterImpl presenter) {
        return presenter;
    }

    @Provides
    @LoginScope
    public Login provideLoginInteractor(LoginImpl login) {
        return login;
    }

    @Provides
    @LoginScope
    public LoginDataSource provideLoginDataSource(LoginDataSourceImpl loginDataSource) {
        return loginDataSource;
    }

    @Provides
    @LoginScope
    public LoginRepository provideLoginRepository(LoginRepositoryImpl loginRepository) {
        return loginRepository;
    }
}
