package com.media.dmitry68.simplegithub.presentation.list;

import com.media.dmitry68.simplegithub.base.BasePresenter;
import com.media.dmitry68.simplegithub.ui.content.list.RepoListView;

public interface RepoListPresenter extends BasePresenter<RepoListView> {
    void setNextRepositories();
}
