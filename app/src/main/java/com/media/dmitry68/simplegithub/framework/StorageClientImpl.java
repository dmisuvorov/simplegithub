package com.media.dmitry68.simplegithub.framework;

import android.content.SharedPreferences;
import android.util.Base64;

import com.media.dmitry68.simplegithub.data.storage.StorageClient;
import com.media.dmitry68.simplegithub.domain.BasicUserAuth;
import com.media.dmitry68.simplegithub.utils.Constants;

import java.nio.charset.StandardCharsets;

import javax.inject.Inject;

public class StorageClientImpl implements StorageClient {
    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    SharedPreferences.Editor editor;

    @Inject
    public StorageClientImpl() {

    }

    @Override
    public BasicUserAuth getUserBasicAuth() {
        return new BasicUserAuth(getBase64String(Constants.KEY_LOGIN), getBase64String(Constants.KEY_PASSWORD));
    }

    @Override
    public void putBase64String(String key, String value) {
        putString(key, Base64.encodeToString(value.getBytes(), Base64.NO_WRAP));
    }

    @Override
    public String getBase64String(String key) {
        return new String(Base64.decode(getString(key), Base64.NO_WRAP), StandardCharsets.UTF_8);
    }

    @Override
    public void putString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    @Override
    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }
}
