package com.media.dmitry68.simplegithub.presentation.login;

import com.media.dmitry68.simplegithub.interactors.Login;
import com.media.dmitry68.simplegithub.ui.login.LoginView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView view;
    private Disposable disposable;

    @Inject
    Login interactor;

    @Inject
    LoginPresenterImpl() {

    }

    @Override
    public void bind(LoginView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        view = null;
    }

    @Override
    public void tryLogin() {
        disposable = interactor.getSavedAuth()
                .subscribe(basicUserAuth -> {
                    if (view != null) {
                        view.setLogin(basicUserAuth.getLogin());
                        view.setPassword(basicUserAuth.getPassword());
                    }
                });

    }

    @Override
    public void login(String login, String password) {
        if (view != null) {
            view.showProgress();
        }

        disposable = interactor.login(login, password)
                .subscribe(user -> {
                            interactor.putAuthenticatedUser(login, password);
                            if (view != null) {
                                view.dismissProgress();
                                view.loginSuccess(user);
                            }
                        },
                        throwable -> {
                            if (view != null) {
                                view.dismissProgress();
                                view.loginFail();
                            }
                        }
                );
    }
}
