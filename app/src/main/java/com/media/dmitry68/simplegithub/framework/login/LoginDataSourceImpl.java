package com.media.dmitry68.simplegithub.framework.login;

import android.util.Base64;

import com.media.dmitry68.simplegithub.data.login.LoginDataSource;
import com.media.dmitry68.simplegithub.data.client.GitHubApi;
import com.media.dmitry68.simplegithub.data.storage.StorageClient;
import com.media.dmitry68.simplegithub.di.LoginScope;
import com.media.dmitry68.simplegithub.domain.BasicUserAuth;
import com.media.dmitry68.simplegithub.domain.User;
import com.media.dmitry68.simplegithub.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@LoginScope
public class LoginDataSourceImpl implements LoginDataSource {
    private String saveLogin;
    private String savePassword;

    @Inject
    GitHubApi gitHubService;

    @Inject
    StorageClient storageClient;

    @Inject
    public LoginDataSourceImpl() {

    }

    @Override
    public Single<BasicUserAuth> getUserBasicAuth() {
        return Single.fromCallable(() -> storageClient.getUserBasicAuth())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Single<User> login(String username, String password) {
        String authHeader = "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);

        return gitHubService.login(authHeader)
                .flatMap(userResponse ->
                        Single.fromCallable(() -> {
                            User user = new User();
                            user.setLogin(userResponse.login);
                            user.setId(userResponse.id);
                            user.setAvatarUrl(userResponse.avatar_url);
                            user.setPublicRepos(userResponse.public_repos);
                            user.setFollowers(userResponse.followers);
                            return user;
                        }))
                .flatMap(user ->
                        gitHubService.getStarredRepositoriesByUser(user.getLogin())
                                .map(List::size)
                                .flatMap(sizeStarred ->
                                        Single.fromCallable(() -> {
                                            user.setStarred(sizeStarred);
                                            return user;
                                        })))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void putAuthenticatedUser(String login, String password) {
        storageClient.putBase64String(Constants.KEY_LOGIN, login);
        storageClient.putBase64String(Constants.KEY_PASSWORD, password);
    }
}
