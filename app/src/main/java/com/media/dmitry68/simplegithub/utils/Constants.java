package com.media.dmitry68.simplegithub.utils;

public class Constants {

    public static final String BASE_URL = "https://api.github.com/";

    public static final String APP_PREFERENCES = "pref";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
}
