package com.media.dmitry68.simplegithub.ui.repo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.base.BaseActivity;
import com.media.dmitry68.simplegithub.di.ApplicationComponent;
import com.media.dmitry68.simplegithub.domain.Repositories;
import com.media.dmitry68.simplegithub.utils.ImageUtil;

public class RepositoryDetailsActivity extends BaseActivity {
    private static final String ARG_REPO = "repoModel";

    public static Intent newIntent(Context context, Repositories.Item repository) {
        Intent intent = new Intent(context, RepositoryDetailsActivity.class);
        intent.putExtra(ARG_REPO, repository);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_detail);
        if (getIntent() == null || getIntent().getExtras() == null || getIntent().getExtras().getParcelable(ARG_REPO) == null) {
            finish();
            return;
        }
        TextView nameRepoTv = findViewById(R.id.name_repo_tv);
        ImageView ownerAvatar = findViewById(R.id.owner_repo_avatar);
        TextView ownerLoginTv = findViewById(R.id.owner_repo_login_tv);
        TextView repoCountForksTv = findViewById(R.id.owner_repo_forks_tv);
        TextView repoCountWatchersTv = findViewById(R.id.owner_repo_watchers_tv);
        TextView repoCountStarsTv = findViewById(R.id.owner_repo_stars_tv);


        Repositories.Item repository = getIntent().getExtras().getParcelable(ARG_REPO);
        if (repository != null) {
            nameRepoTv.setText(repository.getNameRepo());
            ImageUtil.loadAvatarFromUrl(this, repository.getAvatarOwnerUrl(), ownerAvatar);
            ownerLoginTv.setText(repository.getNameOwner());
            repoCountForksTv.setText(String.valueOf(repository.getForkCount()));
            repoCountWatchersTv.setText(String.valueOf(repository.getWatchersCount()));
            repoCountStarsTv.setText(String.valueOf(repository.getStarsCount()));
        }
    }

    @Override
    protected void injectDependencies(App application, ApplicationComponent component) {

    }

    @Override
    protected void releaseSubComponents(App application) {

    }
}
