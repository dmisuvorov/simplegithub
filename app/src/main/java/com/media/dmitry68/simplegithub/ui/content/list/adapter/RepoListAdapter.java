package com.media.dmitry68.simplegithub.ui.content.list.adapter;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.domain.Repositories;

import java.util.ArrayList;
import java.util.List;

public class RepoListAdapter extends RecyclerView.Adapter<RepoListHolder> {

    private AdapterClickListener adapterClickListener;
    private List<Repositories.Item> repositories = new ArrayList<>();

    public RepoListAdapter(AdapterClickListener clickListener) {
        this.adapterClickListener = clickListener;
    }

    @NonNull
    @Override
    public RepoListHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo_list, parent, false);
        return new RepoListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RepoListHolder repoListHolder, int position) {
        repoListHolder.bind(repositories.get(position));
        repoListHolder.itemView.setOnClickListener(v -> onItemClicked(position));
    }

    public void setList(List<Repositories.Item> list) {
        DiffUtil.DiffResult diffResult = diffResult(list);
        repositories = list;
        diffResult.dispatchUpdatesTo(this);
        OnRepoScrollListener.repoScrollLoading = true;
    }

    public void addList(List<Repositories.Item> list) {
        List<Repositories.Item> newList = new ArrayList<>(repositories);
        newList.addAll(list);
        DiffUtil.DiffResult diffResult = diffResult(newList);
        repositories = newList;
        diffResult.dispatchUpdatesTo(this);
        OnRepoScrollListener.repoScrollLoading = true;
    }

    private void onItemClicked(int position) {
        adapterClickListener.onItemClicked(repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    private DiffUtil.DiffResult diffResult(List<Repositories.Item> newList) {
        return DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return repositories.size();
            }

            @Override
            public int getNewListSize() {
                return newList.size();
            }

            @Override
            public boolean areItemsTheSame(int oldPos, int newPos) {
                return repositories.get(oldPos).getId() == newList.get(newPos).getId();
            }

            @Override
            public boolean areContentsTheSame(int oldPos, int newPos) {
                return repositories.get(oldPos).hashCode() == newList.get(newPos).hashCode();
            }
        });
    }

    public interface AdapterClickListener {
        void onItemClicked(Repositories.Item repository);
    }
}
