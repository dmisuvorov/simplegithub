package com.media.dmitry68.simplegithub.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.media.dmitry68.simplegithub.data.storage.StorageClient;
import com.media.dmitry68.simplegithub.framework.StorageClientImpl;
import com.media.dmitry68.simplegithub.utils.Constants;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    @Named("app_pref")
    String provideNameOfPref() {
        return Constants.APP_PREFERENCES;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(@Named("app_pref") String name, Context context) {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SharedPreferences.Editor provideEditorSharedPref(SharedPreferences sharedPreferences) {
        return sharedPreferences.edit();
    }

    @Provides
    @Singleton
    StorageClient provideStorageClient(StorageClientImpl storageClient) {
        return storageClient;
    }
}
