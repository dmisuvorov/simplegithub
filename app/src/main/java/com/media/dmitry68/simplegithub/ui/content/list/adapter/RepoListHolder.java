package com.media.dmitry68.simplegithub.ui.content.list.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.domain.Repositories;
import com.media.dmitry68.simplegithub.utils.ImageUtil;


class RepoListHolder extends RecyclerView.ViewHolder {

    private TextView nameRepo;
    private TextView nameOwner;
    private TextView language;
    private ImageView avatarOwner;

    RepoListHolder(@NonNull View itemView) {
        super(itemView);
        nameRepo = itemView.findViewById(R.id.name_repo);
        nameOwner = itemView.findViewById(R.id.owner);
        language = itemView.findViewById(R.id.language);
        avatarOwner = itemView.findViewById(R.id.avatar_in_repo_list);
    }

    void bind(Repositories.Item repository) {
        nameRepo.setText(repository.getNameRepo());
        nameOwner.setText(repository.getNameOwner());
        language.setText(repository.getLanguage());
        ImageUtil.loadAvatarFromUrl(avatarOwner.getContext(), repository.getAvatarOwnerUrl(), avatarOwner);
    }
}
