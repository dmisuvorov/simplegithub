package com.media.dmitry68.simplegithub.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public final class Repositories {
    private List<Item> list;

    public Repositories() {
        this.list = new ArrayList<>();
    }

    public ArrayList<Item> getList() {
       return (ArrayList<Item>) list;
    }

    public void addItem(Item repository) {
        list.add(repository);
    }

    public void setList(List<Item> list) {
        this.list.addAll(list);
    }

    public static final class Item implements Parcelable {
        private long id;
        private String nameRepo;
        private String nameOwner;
        private String avatarOwnerUrl;
        private String language;
        private int starsCount;
        private int forkCount;
        private int watchersCount;

        public Item() {

        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getNameRepo() {
            return nameRepo;
        }

        public void setNameRepo(String nameRepo) {
            this.nameRepo = nameRepo;
        }

        public String getNameOwner() {
            return nameOwner;
        }

        public void setNameOwner(String nameOwner) {
            this.nameOwner = nameOwner;
        }

        public String getAvatarOwnerUrl() {
            return avatarOwnerUrl;
        }

        public void setAvatarOwnerUrl(String avatarOwnerUrl) {
            this.avatarOwnerUrl = avatarOwnerUrl;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public int getStarsCount() {
            return starsCount;
        }

        public void setStarsCount(int starsCount) {
            this.starsCount = starsCount;
        }

        public int getForkCount() {
            return forkCount;
        }

        public void setForkCount(int forkCount) {
            this.forkCount = forkCount;
        }

        public int getWatchersCount() {
            return watchersCount;
        }

        public void setWatchersCount(int watchersCount) {
            this.watchersCount = watchersCount;
        }

        @Override
        public int describeContents() {
            return 0;
        }



        protected Item(Parcel in) {
            id = in.readLong();
            nameRepo = in.readString();
            nameOwner = in.readString();
            avatarOwnerUrl = in.readString();
            language = in.readString();
            starsCount = in.readInt();
            forkCount = in.readInt();
            watchersCount = in.readInt();
        }

        public static final Creator<Item> CREATOR = new Creator<Item>() {
            @Override
            public Item createFromParcel(Parcel in) {
                return new Item(in);
            }

            @Override
            public Item[] newArray(int size) {
                return new Item[size];
            }
        };


        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(id);
            dest.writeString(nameRepo);
            dest.writeString(nameOwner);
            dest.writeString(avatarOwnerUrl);
            dest.writeString(language);
            dest.writeInt(starsCount);
            dest.writeInt(forkCount);
            dest.writeInt(watchersCount);
        }


        //TODO:

//        @Override
//        public boolean equals(@androidx.annotation.Nullable Object obj) {
//            return super.equals(obj);
//        }
//
//        @Override
//        public int hashCode() {
//            return super.hashCode();
//        }
    }
}
