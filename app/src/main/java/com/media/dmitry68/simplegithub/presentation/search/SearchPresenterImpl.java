package com.media.dmitry68.simplegithub.presentation.search;

import com.media.dmitry68.simplegithub.framework.search.NoSuchRepositoryException;
import com.media.dmitry68.simplegithub.interactors.Search;
import com.media.dmitry68.simplegithub.ui.content.search.SearchView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class SearchPresenterImpl implements SearchPresenter {
    @Inject
    Search interactor;

    private SearchView view;
    private Disposable disposable;
    private String query;
    private static int repoPage = 1;

    @Inject
    public SearchPresenterImpl() {

    }

    @Override
    public void newSearch(boolean isConnected, String query) {
        repoPage = 1;
        doSearch(isConnected, query);
    }

    @Override
    public void doSearch(boolean isConnected, String query) {
        if (view != null) {
            view.showProgress();
        }
        if (isConnected) {
            this.query = query;
            disposable = interactor.fetchSearchRepositories(query, repoPage)
                    .subscribe(repositories -> {
                                if (view != null) {
                                    view.dismissProgress();
                                    if (repoPage == 1) {
                                        view.showRepositories(repositories);
                                    } else {
                                        view.showNextRepositories(repositories);
                                    }
                                }
                            },
                            throwable -> {
                                if (view != null) {
                                    view.dismissProgress();
                                    if (throwable instanceof NoSuchRepositoryException) {
                                        view.showQueryNoResult();
                                    } else {
                                        view.showQueryError(throwable.getMessage());
                                    }
                                }
                            });
        } else if (view != null) {
            view.showOfflineMessage();
        }
    }

    @Override
    public void searchNext(boolean isConnected) {
        repoPage++;
        doSearch(isConnected, query);
    }

    @Override
    public boolean isQueryValid(String query) {
        return query != null && !query.isEmpty();
    }

    @Override
    public void bind(SearchView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        view = null;
    }
}
