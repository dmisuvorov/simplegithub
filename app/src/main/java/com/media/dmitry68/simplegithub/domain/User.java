package com.media.dmitry68.simplegithub.domain;

import android.os.Parcel;
import android.os.Parcelable;

public final class User implements Parcelable {
    private long id;
    private String login;
    private String avatar_url;
    private int public_repos;
    private int followers;
    private int starred;
    public User() {

    }

    protected User(Parcel in) {
        id = in.readLong();
        login = in.readString();
        avatar_url = in.readString();
        public_repos = in.readInt();
        followers = in.readInt();
        starred = in.readInt();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatar_url;
    }

    public void setAvatarUrl(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public int getPublicRepos() {
        return public_repos;
    }

    public void setPublicRepos(int public_repos) {
        this.public_repos = public_repos;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getStarred() {
        return starred;
    }

    public void setStarred(int starred) {
        this.starred = starred;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(login);
        dest.writeString(avatar_url);
        dest.writeInt(public_repos);
        dest.writeInt(followers);
        dest.writeInt(starred);
    }
}
