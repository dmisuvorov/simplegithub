package com.media.dmitry68.simplegithub.ui.content.search;

import com.media.dmitry68.simplegithub.domain.Repositories;


public interface SearchView {
    void showRepositories(Repositories repositories);

    void requestNextRepositories();

    void showNextRepositories(Repositories repositories);

    void showQueryError(String message);

    void showQueryNoResult();

    void showProgress();

    void dismissProgress();

    void showOfflineMessage();
}
