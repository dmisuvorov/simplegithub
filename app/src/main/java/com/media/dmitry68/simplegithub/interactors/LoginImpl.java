package com.media.dmitry68.simplegithub.interactors;

import com.media.dmitry68.simplegithub.data.login.LoginRepository;
import com.media.dmitry68.simplegithub.di.LoginScope;
import com.media.dmitry68.simplegithub.domain.BasicUserAuth;
import com.media.dmitry68.simplegithub.domain.User;

import javax.inject.Inject;

import io.reactivex.Single;

@LoginScope
public class LoginImpl implements Login {

    @Inject
    LoginRepository loginRepository;

    @Inject
    public LoginImpl() {

    }

    @Override
    public Single<BasicUserAuth> getSavedAuth() {
        return loginRepository.getSavedAuth();
    }

    @Override
    public Single<User> login(String login, String password) {
        return loginRepository.login(login, password);
    }

    @Override
    public void putAuthenticatedUser(String login, String password) {
        loginRepository.putAuthenticatedUser(login, password);
    }
}
