package com.media.dmitry68.simplegithub.base;

public interface BasePresenter<T> {
    void bind(T view);

    void unbind();
}
