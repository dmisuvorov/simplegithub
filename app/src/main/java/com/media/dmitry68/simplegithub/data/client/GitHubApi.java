package com.media.dmitry68.simplegithub.data.client;

import com.media.dmitry68.simplegithub.domain.RepositoryResponse;
import com.media.dmitry68.simplegithub.domain.UserResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubApi {

    @GET("user")
    Single<UserResponse> login(@Header("Authorization") String authHeader);

    @GET("users/{user}/starred")
    Single<List<RepositoryResponse.Item>> getStarredRepositoriesByUser(@Path("user") String user);

    @GET("search/repositories")
    Single<RepositoryResponse> searchRepositories(@Query("q") String query, @Query("page") int page);
}
