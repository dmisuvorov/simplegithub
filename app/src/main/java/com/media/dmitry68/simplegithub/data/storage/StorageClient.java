package com.media.dmitry68.simplegithub.data.storage;

import com.media.dmitry68.simplegithub.domain.BasicUserAuth;

public interface StorageClient {
    BasicUserAuth getUserBasicAuth();

    void putBase64String(String key, String value);

    String getBase64String(String key);

    void putString(String key, String value);

    String getString(String key);
}
