package com.media.dmitry68.simplegithub.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.di.ApplicationComponent;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectDependencies(App.get(this), App.getComponent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseSubComponents(App.get(this));
    }

    protected abstract void releaseSubComponents(App application);

    protected abstract void injectDependencies(App application, ApplicationComponent component);
}
