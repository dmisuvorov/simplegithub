package com.media.dmitry68.simplegithub.base;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.media.dmitry68.simplegithub.App;

public abstract class BaseFragment extends Fragment {
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        injectDependencies(App.get(getContext()));
    }

    protected abstract void injectDependencies(App application);
}
