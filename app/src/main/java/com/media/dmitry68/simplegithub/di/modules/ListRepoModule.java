package com.media.dmitry68.simplegithub.di.modules;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

import com.media.dmitry68.simplegithub.di.ListRepoScope;
import com.media.dmitry68.simplegithub.presentation.list.RepoListPresenter;
import com.media.dmitry68.simplegithub.presentation.list.RepoListPresenterImpl;
import com.media.dmitry68.simplegithub.ui.content.list.RepoListFragment;
import com.media.dmitry68.simplegithub.ui.content.list.adapter.OnRepoScrollListener;
import com.media.dmitry68.simplegithub.ui.content.list.adapter.RepoListAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListRepoModule {
    private RepoListFragment repoListFragment;

    public ListRepoModule(RepoListFragment repoListFragment) {
        this.repoListFragment = repoListFragment;
    }

    @Provides
    @ListRepoScope
    RepoListAdapter provideRepoListAdapter(RepoListAdapter.AdapterClickListener clickListener) {
        return new RepoListAdapter(clickListener);
    }

    @Provides
    @ListRepoScope
    RepoListAdapter.AdapterClickListener provideAdapterClickListener() {
        return repoListFragment;
    }

    @Provides
    @ListRepoScope
    OnRepoScrollListener provideScrollListener(LinearLayoutManager linearLayoutManager, RepoListPresenter repoListPresenter) {
        return new OnRepoScrollListener(linearLayoutManager, repoListPresenter);
    }

    @Provides
    @ListRepoScope
    RepoListPresenter provideRepoListPresenter(RepoListPresenterImpl repoListPresenter) {
        return repoListPresenter;
    }

    @Provides
    @ListRepoScope
    LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }
}
