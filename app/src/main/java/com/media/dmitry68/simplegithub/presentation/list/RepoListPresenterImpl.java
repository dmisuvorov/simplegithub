package com.media.dmitry68.simplegithub.presentation.list;

import com.media.dmitry68.simplegithub.ui.content.list.RepoListView;

import javax.inject.Inject;

public class RepoListPresenterImpl implements RepoListPresenter {
    private RepoListView repoListView;

    @Inject
    public RepoListPresenterImpl() {

    }

    @Override
    public void setNextRepositories() {
        if (repoListView != null) {
            repoListView.requestNextRepositories();
        }
    }

    @Override
    public void bind(RepoListView view) {
        repoListView = view;
    }

    @Override
    public void unbind() {
        repoListView = null;
    }
}
