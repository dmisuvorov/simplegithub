package com.media.dmitry68.simplegithub.di;


import com.media.dmitry68.simplegithub.di.modules.ApplicationModule;
import com.media.dmitry68.simplegithub.di.modules.ClientModule;
import com.media.dmitry68.simplegithub.di.modules.ListRepoModule;
import com.media.dmitry68.simplegithub.di.modules.LoginModule;
import com.media.dmitry68.simplegithub.di.modules.NetworkModule;
import com.media.dmitry68.simplegithub.di.modules.SearchModule;
import com.media.dmitry68.simplegithub.di.modules.StorageModule;
import com.media.dmitry68.simplegithub.di.modules.AndroidModule;
import com.media.dmitry68.simplegithub.ui.content.ContentActivity;
import com.media.dmitry68.simplegithub.ui.repo.RepositoryDetailsActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidModule.class,
        ApplicationModule.class,
        ClientModule.class,
        NetworkModule.class,
        StorageModule.class})
public interface ApplicationComponent {
    void inject(ContentActivity activity);

    void inject(RepositoryDetailsActivity activity);

    LoginSubComponent plus(LoginModule module);

    SearchSubComponent plus(SearchModule module);

    ListRepoSubComponent plus(ListRepoModule module);
}
