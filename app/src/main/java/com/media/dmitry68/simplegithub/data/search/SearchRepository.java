package com.media.dmitry68.simplegithub.data.search;

import com.media.dmitry68.simplegithub.domain.Repositories;


import io.reactivex.Single;

public interface SearchRepository {

    Single<Repositories> fetchSearchRepositories(String query, int repoPage);
}
