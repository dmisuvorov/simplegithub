package com.media.dmitry68.simplegithub.ui.content.search;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.base.BaseFragment;
import com.media.dmitry68.simplegithub.domain.Repositories;
import com.media.dmitry68.simplegithub.presentation.search.SearchPresenter;
import com.media.dmitry68.simplegithub.utils.NetworkUtil;


import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class SearchFragment extends BaseFragment implements SearchView {

    @Inject
    SearchPresenter presenter;

    @Inject
    Context context;

    @Inject
    Resources resources;

    private ProgressBar progressBar;
    private AutoCompleteTextView repositoryTextView;

    PublishSubject<Repositories> notifyRepository = PublishSubject.create();
    PublishSubject<Repositories> notifyNextRepository = PublishSubject.create();
    PublishSubject<String> notifyMessage = PublishSubject.create();


    public SearchFragment() {

    }

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.bind(this);
    }

    @Override
    protected void injectDependencies(App application) {
        App.getSearchSubComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        progressBar = Objects.requireNonNull(getView()).findViewById(R.id.progress_search);
        repositoryTextView = getView().findViewById(R.id.repository_text);
        Button searchButton = getView().findViewById(R.id.button_search_repository);
        searchButton.setOnClickListener(v -> onButtonClick());
    }

    private void onButtonClick() {
        repositoryTextView.setError(null);
        String query = repositoryTextView.getText().toString().trim();
        if (presenter.isQueryValid(query)) {
            presenter.newSearch(NetworkUtil.isConnected(context), query);
        } else {
            repositoryTextView.setError(resources.getString(R.string.repository_error));
            repositoryTextView.requestFocus();
        }
    }

    @Override
    public void showRepositories(Repositories repositories) {
        notifyRepository.onNext(repositories);
    }

    @Override
    public void requestNextRepositories() {
        presenter.searchNext(NetworkUtil.isConnected(context));
    }

    @Override
    public void showNextRepositories(Repositories repositories) {
        notifyNextRepository.onNext(repositories);
    }

    @Override
    public void showQueryError(String message) {
        notifyMessage.onNext(message);
    }

    @Override
    public void showQueryNoResult() {
        notifyMessage.onNext(resources.getString(R.string.repository_no_result));
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showOfflineMessage() {
        showQueryError(resources.getString(R.string.message_no_internet));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.unbind();
    }

    public Observable<Repositories> repositoryObservable() {
        return notifyRepository.hide();
    }

    public Observable<String> messageObservable() {
        return notifyMessage.hide();
    }

    public Observable<Repositories> nextRepositoriesObservable() {
        return notifyNextRepository.hide();
    }
}
