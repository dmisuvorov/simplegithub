package com.media.dmitry68.simplegithub.ui.content.list;

import com.media.dmitry68.simplegithub.domain.Repositories;

import java.util.List;

public interface RepoListView {
    void setRepositories(List<Repositories.Item> repositories);

    void requestNextRepositories();

    void nextRepositories(List<Repositories.Item> repositories);
}
