package com.media.dmitry68.simplegithub.di.modules;


import com.media.dmitry68.simplegithub.data.search.SearchDataSource;
import com.media.dmitry68.simplegithub.data.search.SearchRepository;
import com.media.dmitry68.simplegithub.data.search.SearchRepositoryImpl;
import com.media.dmitry68.simplegithub.di.SearchScope;
import com.media.dmitry68.simplegithub.framework.search.SearchDataSourceImpl;
import com.media.dmitry68.simplegithub.interactors.Search;
import com.media.dmitry68.simplegithub.interactors.SearchImpl;
import com.media.dmitry68.simplegithub.presentation.search.SearchPresenter;
import com.media.dmitry68.simplegithub.presentation.search.SearchPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {

    @Provides
    @SearchScope
    public SearchPresenter provideSearchPresenter(SearchPresenterImpl presenter) {
        return presenter;
    }

    @Provides
    @SearchScope
    public Search provideSearchInteractor(SearchImpl search) {
        return search;
    }

    @Provides
    @SearchScope
    public SearchDataSource provideSearchDataSource(SearchDataSourceImpl searchDataSource) {
        return searchDataSource;
    }

    @Provides
    @SearchScope
    public SearchRepository provideSearchRepository(SearchRepositoryImpl searchRepository) {
        return searchRepository;
    }
}
