package com.media.dmitry68.simplegithub.interactors;

import com.media.dmitry68.simplegithub.domain.Repositories;

import io.reactivex.Single;

public interface Search {

    Single<Repositories> fetchSearchRepositories(String query, int repoPage);
}
