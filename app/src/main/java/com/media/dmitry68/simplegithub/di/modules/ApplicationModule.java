package com.media.dmitry68.simplegithub.di.modules;

import com.media.dmitry68.simplegithub.utils.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    HttpUrl provideEndpoint() {
        return HttpUrl.parse(Constants.BASE_URL);
    }
}
