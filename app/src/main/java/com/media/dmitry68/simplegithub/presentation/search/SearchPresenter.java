package com.media.dmitry68.simplegithub.presentation.search;

import com.media.dmitry68.simplegithub.base.BasePresenter;
import com.media.dmitry68.simplegithub.ui.content.search.SearchView;

public interface SearchPresenter extends BasePresenter<SearchView> {

    void newSearch(boolean isConnected, String query);

    void doSearch(boolean isConnected, String query);

    void searchNext(boolean isConnected);

    boolean isQueryValid(String query);
}
