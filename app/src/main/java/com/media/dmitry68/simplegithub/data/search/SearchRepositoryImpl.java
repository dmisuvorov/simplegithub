package com.media.dmitry68.simplegithub.data.search;

import com.media.dmitry68.simplegithub.di.SearchScope;
import com.media.dmitry68.simplegithub.domain.Repositories;

import javax.inject.Inject;

import io.reactivex.Single;

@SearchScope
public class SearchRepositoryImpl implements SearchRepository {
    @Inject
    SearchDataSource searchDataSource;

    @Inject
    public SearchRepositoryImpl () {

    }

    @Override
    public Single<Repositories> fetchSearchRepositories(String query, int repoPage) {
        return searchDataSource.fetchSearchRepositories(query, repoPage);
    }
}
