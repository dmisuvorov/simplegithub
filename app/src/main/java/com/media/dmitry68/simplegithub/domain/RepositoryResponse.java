package com.media.dmitry68.simplegithub.domain;


import java.util.Collection;
import java.util.Date;

public class RepositoryResponse {

    public Collection<Item> items;

    public static final class Item {
        public long id;

        public String name;

        public String fullName;

        public Owner owner;

        public String description;

        public String language;

        public Date createdAt;

        public Date updatedAt;

        public String homepage;

        public int forks_count;

        public int stargazers_count;

        public int watchers;

        public int openIssues;

        public static final class Owner {
            public String login;

            public String avatar_url;
        }
    }
}
