package com.media.dmitry68.simplegithub;

import android.app.Application;
import android.content.Context;

import com.media.dmitry68.simplegithub.di.ApplicationComponent;

import com.media.dmitry68.simplegithub.di.DaggerApplicationComponent;
import com.media.dmitry68.simplegithub.di.ListRepoSubComponent;
import com.media.dmitry68.simplegithub.di.LoginSubComponent;
import com.media.dmitry68.simplegithub.di.SearchSubComponent;
import com.media.dmitry68.simplegithub.di.modules.AndroidModule;
import com.media.dmitry68.simplegithub.di.modules.ListRepoModule;
import com.media.dmitry68.simplegithub.di.modules.LoginModule;
import com.media.dmitry68.simplegithub.di.modules.SearchModule;
import com.media.dmitry68.simplegithub.ui.content.list.RepoListFragment;

public class App extends Application {
    private static ApplicationComponent component;

    private static LoginSubComponent loginSubComponent;
    private static SearchSubComponent searchSubComponent;
    private static ListRepoSubComponent listRepoSubComponent;

    public static ApplicationComponent getComponent() {
        return component;
    }

    public static LoginSubComponent getLoginSubComponent() {
        if (loginSubComponent == null) {
            createLoginSubComponent();
        }
        return loginSubComponent;
    }

    private static void createSearchSubComponent() {
        searchSubComponent = component.plus(new SearchModule());
    }

    public static SearchSubComponent getSearchSubComponent() {
        if (searchSubComponent == null) {
            createSearchSubComponent();
        }
        return searchSubComponent;
    }

    private static void createLoginSubComponent() {
        loginSubComponent = component.plus(new LoginModule());
    }

    public static ListRepoSubComponent getListRepoSubComponent(RepoListFragment repoListFragment) {
        if (listRepoSubComponent == null) {
            createListRepoSubComponent(repoListFragment);
        }
        return listRepoSubComponent;
    }

    private static void createListRepoSubComponent(RepoListFragment repoListFragment) {
        listRepoSubComponent = component.plus(new ListRepoModule(repoListFragment));
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = createComponent();
    }

    private ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }

    public static void releaseLoginSubComponent() {
        loginSubComponent = null;
    }

    public static void releaseSearchSubComponent() {
        searchSubComponent = null;
    }

    public static void releaseListRepoSubComponent() {
        listRepoSubComponent = null;
    }
}
