package com.media.dmitry68.simplegithub.presentation.login;

import com.media.dmitry68.simplegithub.base.BasePresenter;
import com.media.dmitry68.simplegithub.ui.login.LoginView;

public interface LoginPresenter extends BasePresenter<LoginView> {
    void tryLogin();

    void login(String login, String password);
}
