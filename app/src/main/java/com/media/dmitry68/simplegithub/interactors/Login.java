package com.media.dmitry68.simplegithub.interactors;

import com.media.dmitry68.simplegithub.domain.BasicUserAuth;
import com.media.dmitry68.simplegithub.domain.User;

import io.reactivex.Single;

public interface Login {
    Single<BasicUserAuth> getSavedAuth();

    Single<User> login(String login, String password);

    void putAuthenticatedUser(String login, String password);
}
