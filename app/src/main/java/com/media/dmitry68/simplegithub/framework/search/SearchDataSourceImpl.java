package com.media.dmitry68.simplegithub.framework.search;

import com.media.dmitry68.simplegithub.data.client.GitHubApi;
import com.media.dmitry68.simplegithub.data.search.SearchDataSource;
import com.media.dmitry68.simplegithub.di.SearchScope;
import com.media.dmitry68.simplegithub.domain.Repositories;
import com.media.dmitry68.simplegithub.domain.RepositoryResponse;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;

@SearchScope
public class SearchDataSourceImpl implements SearchDataSource {
    @Inject
    GitHubApi gitHubService;

    @Inject
    public SearchDataSourceImpl() {

    }

    @Override
    public Single<Repositories> fetchSearchRepositories(String query, int repoPage) {
        return gitHubService.searchRepositories(query, repoPage)
                .flatMap(repositoryResponses ->
                        Single.fromCallable(() -> {
                            Repositories repositories = new Repositories();
                            for (RepositoryResponse.Item repositoryResponse : repositoryResponses.items) {
                                Repositories.Item repository = new Repositories.Item();
                                repository.setId(repositoryResponse.id);
                                repository.setNameRepo(repositoryResponse.name);
                                repository.setNameOwner(repositoryResponse.owner.login);
                                repository.setAvatarOwnerUrl(repositoryResponse.owner.avatar_url);
                                repository.setLanguage(repositoryResponse.language);
                                repository.setStarsCount(repositoryResponse.stargazers_count);
                                repository.setForkCount(repositoryResponse.forks_count);
                                repository.setWatchersCount(repositoryResponse.watchers);
                                repositories.addItem(repository);
                            }
                            return repositories;
                        }))
                .map(repository -> {
                    if (repository.getList().isEmpty()) {
                        throw Exceptions.propagate(new NoSuchRepositoryException());
                    } else {
                        return repository;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
