package com.media.dmitry68.simplegithub.interactors;

import com.media.dmitry68.simplegithub.data.search.SearchRepository;
import com.media.dmitry68.simplegithub.di.SearchScope;
import com.media.dmitry68.simplegithub.domain.Repositories;

import javax.inject.Inject;

import io.reactivex.Single;

@SearchScope
public class SearchImpl implements Search {
    @Inject
    SearchRepository searchRepository;

    @Inject
    public SearchImpl() {

    }

    @Override
    public Single<Repositories> fetchSearchRepositories(String query, int repoPage) {
        return searchRepository.fetchSearchRepositories(query, repoPage);
    }
}
