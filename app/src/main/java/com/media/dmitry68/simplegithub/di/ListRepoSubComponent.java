package com.media.dmitry68.simplegithub.di;

import com.media.dmitry68.simplegithub.di.modules.ListRepoModule;
import com.media.dmitry68.simplegithub.ui.content.list.RepoListFragment;

import dagger.Subcomponent;

@ListRepoScope
@Subcomponent(modules = ListRepoModule.class)
public interface ListRepoSubComponent {
    void inject(RepoListFragment fragment);
}
