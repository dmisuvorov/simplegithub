package com.media.dmitry68.simplegithub.ui.content;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.base.BaseActivity;
import com.media.dmitry68.simplegithub.di.ApplicationComponent;
import com.media.dmitry68.simplegithub.domain.Repositories;
import com.media.dmitry68.simplegithub.domain.User;
import com.media.dmitry68.simplegithub.ui.content.list.RepoListFragment;
import com.media.dmitry68.simplegithub.ui.content.list.adapter.OnRepoScrollListener;
import com.media.dmitry68.simplegithub.ui.content.search.SearchFragment;
import com.media.dmitry68.simplegithub.ui.content.user.UserDetailsFragment;
import com.media.dmitry68.simplegithub.ui.repo.RepositoryDetailsActivity;

import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ContentActivity extends BaseActivity implements RepoListFragment.RepoListFragmentCallback,
        OnRepoScrollListener.OnScrollListCallback,
        FragmentManager.OnBackStackChangedListener {
    private static final String ARG_USER = "userModel";

    public static final String TAG_USER_DETAILS_FRAGMENT = "user_details_fragment";
    public static final String TAG_SEARCH_FRAGMENT = "repositories_search_fragment";
    private static final String TAG_REPO_LIST_FRAGMENT = "repo_list_fragment";

    private SearchFragment searchFragment;
    private UserDetailsFragment userDetailsFragment;
    private RepoListFragment listFragment;
    private FragmentManager fragmentManager;

    private CompositeDisposable compositeDisposable;

    @Inject
    Context context;

    public static Intent newIntent(Context context, User user) {
        Intent intent = new Intent(context, ContentActivity.class);
        intent.putExtra(ARG_USER, user);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity);
        if (getIntent() == null || getIntent().getExtras() == null || getIntent().getExtras().getParcelable(ARG_USER) == null) {
            finish();
            return;
        }
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
        if (savedInstanceState == null) {
            User user = getIntent().getExtras().getParcelable(ARG_USER);
            userDetailsFragment = UserDetailsFragment.newInstance(user);
            searchFragment = SearchFragment.newInstance();
            attachFragments();
        } else {
            userDetailsFragment = (UserDetailsFragment) fragmentManager.findFragmentByTag(TAG_USER_DETAILS_FRAGMENT);
            searchFragment = (SearchFragment) fragmentManager.findFragmentByTag(TAG_SEARCH_FRAGMENT);
            listFragment = (RepoListFragment) fragmentManager.findFragmentByTag(TAG_REPO_LIST_FRAGMENT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();

            compositeDisposable.addAll(
                    searchFragment.repositoryObservable()
                            .subscribe(this::showRepositories),
                    searchFragment.nextRepositoriesObservable()
                            .subscribe(this::nextRepositories),
                    searchFragment.messageObservable()
                            .subscribe(this::showMessage)
            );
        }
    }

    private void nextRepositories(Repositories repositories) {
        if (listFragment != null) {
            listFragment.nextRepositories(repositories.getList());
        }
    }

    private void showMessage(String message) {
        Snackbar.make(Objects.requireNonNull(getCurrentFocus()), message, Snackbar.LENGTH_SHORT).show();
    }

    private void showRepositories(Repositories repositories) {
        if (listFragment == null) {
            listFragment = RepoListFragment.newInstance(repositories);
            attachListFragment();
        } else {
            attachListFragment();
            listFragment.setRepositories(repositories.getList());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        compositeDisposable.dispose();
    }

    private void attachFragments() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.search_fragment, searchFragment, TAG_SEARCH_FRAGMENT);
        fragmentTransaction.replace(R.id.user_details_fragment, userDetailsFragment, TAG_USER_DETAILS_FRAGMENT);
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void attachListFragment() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.user_details_fragment, listFragment, TAG_REPO_LIST_FRAGMENT);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    protected void releaseSubComponents(App application) {
        App.releaseSearchSubComponent();
        App.releaseListRepoSubComponent();
    }

    @Override
    protected void injectDependencies(App application, ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    public void onScrollList() {
        if (searchFragment != null) {
            searchFragment.requestNextRepositories();
        }
    }

    @Override
    public void onBackStackChanged() {
        if (fragmentManager.findFragmentById(R.id.user_details_fragment) instanceof UserDetailsFragment) {
            listFragment = null;
            App.releaseListRepoSubComponent();
        }
    }

    @Override
    public void showDetailRepository(Repositories.Item repository) {
        startActivity(RepositoryDetailsActivity.newIntent(context, repository));
    }
}
