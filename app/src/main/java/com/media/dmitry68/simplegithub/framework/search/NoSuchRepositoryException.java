package com.media.dmitry68.simplegithub.framework.search;

import java.util.NoSuchElementException;

public class NoSuchRepositoryException extends NoSuchElementException {
}
