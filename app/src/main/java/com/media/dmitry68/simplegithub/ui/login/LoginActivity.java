package com.media.dmitry68.simplegithub.ui.login;

import android.content.Context;
import android.content.res.Resources;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.base.BaseActivity;
import com.media.dmitry68.simplegithub.di.ApplicationComponent;
import com.media.dmitry68.simplegithub.domain.User;
import com.media.dmitry68.simplegithub.presentation.login.LoginPresenter;
import com.media.dmitry68.simplegithub.ui.content.ContentActivity;

import java.util.Objects;

import javax.inject.Inject;


public class LoginActivity extends BaseActivity implements LoginView {

    @Inject
    Context context;

    @Inject
    Resources resources;

    @Inject
    LoginPresenter loginPresenter;

    private TextInputEditText usernameET;
    private TextInputEditText passwordET;
    private TextInputLayout usernameLayout;
    private TextInputLayout passwordLayout;

    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameET = findViewById(R.id.login_username_et);
        passwordET = findViewById(R.id.login_password_et);
        usernameLayout = findViewById(R.id.login_username_layout);
        passwordLayout = findViewById(R.id.login_password_layout);
        AppCompatButton loginButton = findViewById(R.id.login_button);
        progressBar = findViewById(R.id.progress);

        usernameLayout.setHint(getString(R.string.login_hint));
        passwordLayout.setHint(getString(R.string.password_hint));
        usernameLayout.setErrorEnabled(true);
        passwordLayout.setErrorEnabled(true);
        loginButton.setOnClickListener(v -> onClickLoginButton());
    }

    @Override
    protected void onResume() {
        super.onResume();
        loginPresenter.bind(this);
        loginPresenter.tryLogin();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (loginPresenter != null) {
            loginPresenter.unbind();
        }
    }

    @Override
    protected void releaseSubComponents(App application) {
        application.releaseLoginSubComponent();
    }

    @Override
    protected void injectDependencies(App application, ApplicationComponent component) {
        App.getLoginSubComponent().inject(this);
    }

    @Override
    public void setLogin(String login) {
        usernameET.setText(login);
    }

    @Override
    public void setPassword(String password) {
        passwordET.setText(password);
    }

    @Override
    public void onClickLoginButton() {
        String username = Objects.requireNonNull(usernameET.getText()).toString().trim();
        String password = Objects.requireNonNull(passwordET.getText()).toString().trim();
        if (TextUtils.isEmpty(username)) {
            usernameLayout.setError(getString(R.string.message_input_username));
            return;
        }
        if (TextUtils.isEmpty(password)) {
            passwordLayout.setError(getString(R.string.message_input_password));
            return;
        }
        loginPresenter.login(username, password);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void loginSuccess(User user) {
        startActivity(ContentActivity.newIntent(context, user));
    }

    @Override
    public void loginFail() {
        Snackbar.make(Objects.requireNonNull(getCurrentFocus()), resources.getString(R.string.message_login_error), Snackbar.LENGTH_SHORT).show();
    }
}
