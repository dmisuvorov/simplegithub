package com.media.dmitry68.simplegithub.di;

import com.media.dmitry68.simplegithub.di.modules.SearchModule;
import com.media.dmitry68.simplegithub.ui.content.search.SearchFragment;

import dagger.Subcomponent;

@SearchScope
@Subcomponent(modules = SearchModule.class)
public interface SearchSubComponent {

    void inject(SearchFragment fragment);

}

