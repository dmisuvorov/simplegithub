package com.media.dmitry68.simplegithub.ui.content.user;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.media.dmitry68.simplegithub.App;
import com.media.dmitry68.simplegithub.R;
import com.media.dmitry68.simplegithub.base.BaseFragment;
import com.media.dmitry68.simplegithub.domain.User;
import com.media.dmitry68.simplegithub.utils.ImageUtil;

import java.util.Objects;

public class UserDetailsFragment extends BaseFragment {
    private static final String ARG_USER = "userModel";

    private ImageView userAvatar;
    private TextView userLoginTv;
    private TextView userCountFollowerTv;
    private TextView userCountRepositoriesTv;
    private TextView userCountStarsTv;

    public UserDetailsFragment() {

    }

    public static UserDetailsFragment newInstance(User user) {
        UserDetailsFragment userDetailsFragment = new UserDetailsFragment();
        Bundle userBundle = new Bundle();
        userBundle.putParcelable(ARG_USER, user);
        userDetailsFragment.setArguments(userBundle);
        return userDetailsFragment;
    }

    @Override
    protected void injectDependencies(App application) {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        userAvatar = Objects.requireNonNull(getView()).findViewById(R.id.user_avatar);
        userLoginTv = getView().findViewById(R.id.user_login_tv);
        userCountFollowerTv = getView().findViewById(R.id.user_follower_tv);
        userCountRepositoriesTv = getView().findViewById(R.id.user_repositories_tv);
        userCountStarsTv = getView().findViewById(R.id.user_stars_tv);

        if (getArguments() != null) {
            initViews(getArguments().getParcelable(ARG_USER));
        }
    }

    private void initViews(User user) {
        if (user != null) {
            ImageUtil.loadAvatarFromUrl(getContext(), user.getAvatarUrl(), userAvatar);
            userLoginTv.setText(user.getLogin());
            userCountFollowerTv.setText(String.valueOf(user.getFollowers()));
            userCountRepositoriesTv.setText(String.valueOf(user.getPublicRepos()));
            userCountStarsTv.setText(String.valueOf(user.getStarred()));
        }
    }
}
